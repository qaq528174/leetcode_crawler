本项目采用GPLv3协议。

小组的基本分工：
1. 基础代码、SQL链接、并发、演示（ShigureSuki）
2. Excel输出支持（SPP）
3. Markdown输出支持（SK）
4. UI支持（可选，待定）
5. PPT、其他（待定）