#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sqlite3
import json
import html2text
import os
import requests
from requests_toolbelt import MultipartEncoder
import random
import time
import re
import argparse
import sys
import threading
import logging

db_path = 'leetcode.db'
user_agent = r'Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0'
max_thread_num = 12
threadLock = threading.Lock()


def initLock(l):
    global lock
    lock = l


class DatabaseWriterThread(threading.Thread):
    """
    线程类，获取题目信息
    """
    def __init__(self, title_slug, *args):
        threading.Thread.__init__(self)
        self.title_slug = title_slug
        self.status = args[0]
        self.crawler = args[1]

    def run(self):
        success = False
        conn = sqlite3.connect(db_path, timeout=10)
        while not success:
            try:
                # sleep for 1~3 seconds randomly in order not to be banned by server
                time.sleep(random.randint(1, 3))

                cursor = conn.cursor()
                session = requests.Session()
                headers = {'User-Agent': user_agent, 'Connection': 'keep-alive',
                           'Content-Type': 'application/json',
                           'Referer': 'https://leetcode.com/problems/' + self.title_slug}

                url = "https://leetcode.com/graphql"
                params = {'operationName': "getQuestionDetail",
                          'variables': {'titleSlug': self.title_slug},
                          'query': '''query getQuestionDetail($titleSlug: String!) {
                        question(titleSlug: $titleSlug) {
                            questionId
                            questionFrontendId
                            questionTitle
                            questionTitleSlug
                            content
                            difficulty
                            stats
                            similarQuestions
                            categoryTitle
                            topicTags {
                            name
                            slug
                        }
                    }
                }'''
                          }

                json_data = json.dumps(params).encode('utf8')

                try:
                    resp = session.post(url, data=json_data, headers=headers, timeout=10)
                    content = resp.json()
                except json.decoder.JSONDecodeError:
                    logging.warning(f"Grasping of the problem {self.title_slug} failed. Try again later.")
                    crawler.failcnt += 1
                pid = content['data']['question']['questionId']
                tags = [tag['name'] for tag in content['data']['question']['topicTags']]

                if content['data']['question']['content'] is not None:
                    pdetail = (pid,
                               content['data']['question']['questionFrontendId'],
                               content['data']['question']['questionTitle'],
                               content['data']['question']['questionTitleSlug'],
                               content['data']['question']['difficulty'],
                               content['data']['question']['content'],
                               self.status)
                    threadLock.acquire()
                    cursor.execute(
                        'INSERT INTO problems (id, frontend_id, title, slug, difficulty, content, status) VALUES (?, ?, ?, ?, ?, ?, ?)',
                        pdetail)
                    for tag in tags:
                        problem_tags = (pid, tag)
                        cursor.execute('INSERT INTO problem_tags (problem_id, tag) VALUES (?, ?)', problem_tags)
                    conn.commit()
                    logging.info(f"Insert problem [{self.title_slug}] success")
                    crawler.optcnt += 1
                    threadLock.release()
                    success = True
            # 若出现连接超时或连接错误则继续获取
            except (requests.exceptions.Timeout, requests.exceptions.ConnectionError) as error:
                print(str(error))
        cursor.close()
        conn.close()


class LeetcodeCrawler:
    def __init__(self):
        self.session = requests.Session()
        self.csrftoken = ''
        self.parameters = {}
        self.is_login = False
        self.conn = None  # 爬虫相关的数据库连接
        self.optcnt = 0
        self.failcnt = 0
    """
    根据获得的参数初始化爬虫。如果出现非法参数，中止初始化并抛出异常。
    """

    def init(self, args_dict):
        if args_dict.get('verbose'):
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.INFO)
        if args_dict.get('directory'):
            self.parameters['directory'] = args_dict['directory']
            logging.debug(f"Specified output directory = {self.parameters['directory']}")

        self.get_csrftoken()

        if args_dict.get('code') or args_dict.get('stat'):
            if not (args_dict.get('name') and args_dict.get('passwd')):
                raise ValueError("It is necessary to provide username and password when fetching status "
                                 "or submission code")
            if not self.login(args_dict['name'], args_dict['passwd']):
                raise ValueError("Login failure. Check username and password")
            self.parameters['login'] = True
            if args_dict.get('code'):
                self.parameters['path'] = args_dict['code']
                logging.debug(f"Specified code path = {self.parameters['path']}")
            if args_dict.get('stat'):
                self.parameters['status'] = args_dict['stat']
                logging.debug(f"Specified code path = {self.parameters['status']}")
        if args_dict.get('lv') or argsDict.get('tags'):
            if args_dict.get('lv'):
                self.parameters["difficulty"] = args_dict['lv']
                logging.debug(f"Specified difficulty = {self.parameters['difficulty']}")
            if args_dict.get('tags'):
                self.parameters["tags"] = args_dict['tags']
                logging.debug(f"Specified tag = {self.parameters['tags']}")

    """
    针对反爬虫的措施：获取token。
    """

    def get_csrftoken(self):
        url = 'https://leetcode.com'
        cookies = self.session.get(url).cookies
        for cookie in cookies:
            if cookie.name == 'csrftoken':
                self.csrftoken = cookie.value
                break


    def login(self, username, password):
        """
        使用requests库获取登陆leetcode的session。
        """
        url = "https://leetcode.com/accounts/login"

        me = MultipartEncoder({
            'csrfmiddlewaretoken': self.csrftoken,
            'login': username,
            'password': password,
            'next': 'problems'
        })

        headers = {'User-Agent': user_agent, 'Connection': 'keep-alive',
                   'Referer': 'https://leetcode.com/accounts/login/', 'origin': "https://leetcode.com",
                   'Content-Type': me.content_type}

        self.session.post(url, headers=headers, data=me, timeout=10, allow_redirects=False)
        logging.debug(f"cookies: {self.session.cookies.get('LEETCODE_SESSION')}")
        self.is_login = self.session.cookies.get('LEETCODE_SESSION') is not None
        if self.is_login:
            logging.info("Login Successful")
        return self.is_login

    def get_problems(self):
        url = "https://leetcode.com/api/problems/all/"

        headers = {'User-Agent': user_agent, 'Connection': 'keep-alive'}
        resp = self.session.get(url, headers=headers, timeout=10)

        problems_list = json.loads(resp.content.decode('utf-8'))

        prob_upd_list = []
        threads = []

        cursor = self.conn.cursor()

        difficulty_dict = {1: "Easy", 2: "Medium", 3: "Hard"}

        for problem in problems_list['stat_status_pairs']:
            pid = problem['stat']['question_id']
            pslug = problem['stat']['question__title_slug']
            pstat = problem['status']

            pdiff = difficulty_dict.get(problem['difficulty']['level'], "None")

            if self.parameters.get('difficulty') and pdiff not in self.parameters['difficulty']:
                continue

            if self.parameters.get('status') and pstat not in self.parameters['status']:
                continue

            if problem['paid_only']:
                continue

            logging.debug(f"Trying to fetch, ID:{pid}, SLUG:{pslug}, STAT:{pstat}, DIFF:{pdiff}")
            cursor.execute('SELECT status FROM problems WHERE id = ?', (pid,))
            result = cursor.fetchone()
            if not result:
                # 创建新线程
                thread = DatabaseWriterThread(pslug, pstat, self)
                thread.start()
                while True:
                    # 判断正在运行的线程数量,如果小于5则退出while循环,
                    # 进入for循环启动新的进程.否则就一直在while循环进入死循环
                    if len(threading.enumerate()) < max_thread_num:
                        break
                # 添加线程到线程列表
                threads.append(thread)
            elif self.is_login and pstat != result[0]:
                prob_upd_list.append((pstat, pid))
        for t in threads:
            t.join()

        cursor.executemany('UPDATE problems SET status = ? WHERE id = ?', prob_upd_list)
        self.conn.commit()
        cursor.close()

        logging.info(f"{self.optcnt} operations finished, {self.failcnt} operations failed.")

    def connect_db(self, db_path):
        self.conn = sqlite3.connect(db_path, timeout=10)
        cursor = self.conn.cursor()

        # Table 'problems'
        cursor.execute("SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name = 'problems';")
        if cursor.fetchone()[0] == 0:
            cursor.execute('''CREATE TABLE problems
                    (id                INT      PRIMARY KEY     NOT NULL,
                    frontend_id        INT                      NOT NULL,
                    title              CHAR(50)                 NOT NULL,
                    slug               CHAR(50)                 NOT NULL,
                    difficulty         CHAR(10)                 NOT NULL,
                    content            TEXT                     NOT NULL,
                    status             CHAR(10));''')

        # Table 'last_ac_submission_record'
        cursor.execute("SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name = 'last_ac_submission_record';")
        if cursor.fetchone()[0] == 0:
            cursor.execute('''CREATE TABLE last_ac_submission_record
                    (id      INT PRIMARY KEY       NOT NULL,
                    problem_slug      CHAR(50)    NOT NULL,
                    timestamp          INT         NOT NULL,
                    language         CHAR(10)      NOT NULL,
                    code               TEXT,
                    runtime            CHAR(10));''')

        # Table 'problem_tags'
        cursor.execute("SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name = 'problem_tags';")
        if cursor.fetchone()[0] == 0:
            cursor.execute('''CREATE TABLE problem_tags
                    (problem_id      INT       NOT NULL,
                    tag      CHAR(30)    NOT NULL);''')

        cursor.close()

    def generate_problems_excel(self):
        pass

    def generate_problems_markdown(self):
        pass

    def generate_problem_markdown(self, question, path, has_get_code):
        pass

    def generate_problem_excel(self, question, path, has_get_code):
        pass

    def filter_problems(self, p_detail):
        if self.parameters.get('difficulty') and self.parameters['difficulty'] != p_detail['difficulty']:
            return False
        if self.parameters.get('status') and self.parameters['status'] != p_detail['status']:
            return False

        tag_cursor = self.conn.cursor()
        tag_cursor.execute('SELECT tag FROM problem_tags WHERE problem_id = ?', (p_detail['id'],))
        tag_list = tag_cursor.fetchall()
        tag_cursor.close()
        if self.parameters.get('tags'):
            tag_count = sum([1 for tag in tag_list if tag[0] in self.parameters['tags']])
            if tag_count != len(self.parameters['tags']):
                return False
        return True

    def get_ac_problem_submission(self):
        if not self.is_login:
            logging.warning(f"account not given, so submission files cannot be fetched")
            return
        sql = "SELECT id,slug,difficulty,status FROM problems WHERE status = 'ac';"
        cursor = self.conn.cursor()
        cursor.execute(sql)
        results = cursor.fetchall()

        # threads = []

        slug_list = []
        for row in results:
            question_detail = {
                'id': row[0],
                'slug': row[1],
                'difficulty': row[2],
                'status': row[3]
            }

            if not self.filter_problems(question_detail):
                continue

            slug = question_detail['slug']
            slug_list.append(slug)
            is_ok = False
            while not is_ok:
                try:
                    url = "https://leetcode.com/graphql"
                    params = {'operationName': "Submissions",
                              'variables': {"offset": 0, "limit": 20, "lastKey": '', "questionSlug": slug},
                              'query': '''query Submissions($offset: Int!, $limit: Int!, $lastKey: String, $questionSlug: String!) {
                                submissionList(offset: $offset, limit: $limit, lastKey: $lastKey, questionSlug: $questionSlug) {
                                lastKey
                                hasNext
                                submissions {
                                    id
                                    statusDisplay
                                    lang
                                    runtime
                                    timestamp
                                    url
                                    isPending
                                    __typename
                                }
                                __typename
                            }
                        }'''
                              }

                    json_data = json.dumps(params).encode('utf8')

                    headers = {'User-Agent': user_agent, 'Connection': 'keep-alive',
                               'Referer': 'https://leetcode.com/accounts/login/',
                               "Content-Type": "application/json", 'x-csrftoken': self.csrftoken}
                    resp = self.session.post(url, data=json_data, headers=headers, timeout=10)
                    content = resp.json()
                    for submission in content['data']['submissionList']['submissions']:
                        if submission['statusDisplay'] == "Accepted":
                            cursor.execute(
                                "SELECT COUNT(*) FROM last_ac_submission_record WHERE id =" + str(submission['id']))
                            if cursor.fetchone()[0] == 0:
                                submission_get_success = False
                                while not submission_get_success:
                                    code_content = self.session.get("https://leetcode.com" + submission['url'],
                                                                    headers=headers, timeout=10)
                                    pattern = re.compile(r'submissionCode: \'(?P<code>.*)\',\n  editCodeUrl', re.S)
                                    m1 = pattern.search(code_content.text)
                                    code = m1.groupdict()['code'] if m1 else None
                                    if not code:
                                        logging.warning(f"Can not get [{slug}] solution code")
                                        continue
                                    submission_get_success = True

                                submission_detail = (submission['id'],
                                                     slug,
                                                     submission['timestamp'],
                                                     submission['lang'],
                                                     submission['runtime'],
                                                     code)
                                cursor.execute(
                                    "INSERT INTO last_ac_submission_record (id, problem_slug, timestamp, language, runtime, code) VALUES(?, ?, ?, ?, ?, ?)",
                                    submission_detail)
                                logging.info("Insert submission[%s] success" % (submission['id']))
                                self.conn.commit()
                            is_ok = True
                            break
                except (requests.exceptions.Timeout, requests.exceptions.ConnectionError) as error:
                    logging.error(error)
                finally:
                    pass
        cursor.close()

    def generate_problems_submission(self):
        if not self.is_login:
            logging.warning(f"account not given, so submission files cannot be fetched")
            return

        sql = """
            SELECT l.problem_slug, l.code,l.language, p.frontend_id, max(l.timestamp) FROM last_ac_submission_record as l, problems as p 
            WHERE l.problem_slug == p.slug and p.status = 'ac' GROUP BY l.problem_slug
        """
        cursor = self.conn.cursor()
        cursor.execute(sql)

        filter_cursor = self.conn.cursor()
        for submission in cursor:
            filter_cursor.execute("SELECT id,slug,difficulty,status FROM problems WHERE slug = ?", (submission[0],))
            result = filter_cursor.fetchone()
            if not self.filter_problems({
                'id': result[0],
                'slug': result[1],
                'difficulty': result[2],
                'status': result[3]
            }):
                continue
            self.generate_problem_submission(self.parameters['directory'], submission)

        cursor.close()
        filter_cursor.close()

    def generate_problem_submission(self, path, submission):
        if not os.path.isdir(path):
            os.mkdir(path)

        text_path = os.path.join(path, "{:0>3d}-{}".format(submission[3], submission[0]))

        if not os.path.isdir(text_path):
            os.mkdir(text_path)
        with open(os.path.join(text_path, "solution.txt"), 'w', encoding='utf-8') as f:
            f.write(submission[1].encode('utf-8').decode('unicode_escape'))

    def close_db(self):
        self.conn.close()


def arg_parser(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("directory", nargs="?", default="notes",
                        help="Specify the output directory.\nIf not, output will be stored in directory 'notes'.")
    parser.add_argument("-d", "--difficulty", nargs="+", dest="lv", choices=["Easy", "Medium", "Hard"],
                        help="Specify the difficulty.\nIf not, problems of all levels will be downloaded.")
    parser.add_argument("-t", "--tags", nargs="+",
                        help="Specify the tag(s) of the problems.")
    parser.add_argument('-v', '--verbose', action="store_true", default=False,
                        help="Verbose output.")
    parser.add_argument('-s', '--status', nargs='+', dest="stat", choices=['ac', 'notac', 'none'],
                        help="Limit the status of problems.\nIf not specified, all problems will be grasped.")
    parser.add_argument('-c', '--code', nargs='?',
                        help="Specify the path of code directory.")
    parser.add_argument('-u', '--username', nargs='?', dest="name",
                        help="Specify the logging name of leetcode.com")
    parser.add_argument('-p', '--password', nargs='?', dest="passwd",
                        help="Specify the logging password of leetcode.com")

    if len(argv) > 1:
        return vars(parser.parse_args())
    else:
        parser.print_help()
        sys.exit(1)


if __name__ == '__main__':
    argsDict = arg_parser(sys.argv)
    crawler = LeetcodeCrawler()
    crawler.init(argsDict)

    crawler.connect_db(db_path)
    crawler.get_problems()

    if argsDict.get('code'):
        crawler.get_ac_problem_submission()
        crawler.generate_problems_submission()

    # crawler.generate_problems_markdown()  # TODO: 待完成
    # crawler.generate_problems_excel()  # TODO: 待完成

    crawler.close_db()
